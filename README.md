# Kraut Chat

This is a PoC (Psycho-ordered-Chaos) of the chat based on a distributed graph DB.

## How to run

### Run with Cassandra in local mode
1) `docker run -d -e CASSANDRA_START_RPC=true -p 9160:9160 -p 9042:9042 -p 7199:7199 -p 7001:7001 -p 7000:7000 cassandra:3.11`
2) `docker run -it -p 8182:8182 janusgraph/janusgraph`

### Run with HBase in local mode
0) Change code to use `HBaseGraphDb` instead of `CassandraGraphDb`.
1) Download and extract HBase.
2) Run `start-hbase.sh` from the "bin" directory of the extracted archive.
3) `docker run -it -p 8182:8182 janusgraph/janusgraph`

### Run application
- Run
`./gradlew clean assembleDist`
- Extract the archive located in the `./build/distributions` directory.
- Run the `sh`/`bat` script located in the `bin` directory of the extracted archive. For example, `./KrautChat -s /path/to/static/content`

---
### Useful resources
- [JanusGraph docs](https://docs.janusgraph.org/)
- [Tinkerpop/Gremlin site](https://tinkerpop.apache.org/) and [docs](https://tinkerpop.apache.org/docs/current/)
- ["Practical Gremlin"](https://www.kelvinlawrence.net/book/Gremlin-Graph-Guide.html)
