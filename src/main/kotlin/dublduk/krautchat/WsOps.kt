package dublduk.krautchat

import dublduk.krautchat.model.User
import io.javalin.websocket.WsContext
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json

private val SESSIONS = mutableSetOf<WsContext>()

fun WsContext.addClient(message: String, recipients: List<User>) {
    SESSIONS.add(this)
    serverPush(message, recipients)
}

fun WsContext.removeClient(message: String, recipients: List<User>) {
    SESSIONS.remove(this)
    serverPush(message, recipients)
}

fun sendMessage(sender: String, message: String, recipients: List<User>) {
    broadcastMessage(sender, message, recipients)
}

private fun serverPush(message: String, recipients: List<User>) {
    broadcastMessage("Server", message, recipients)
}

private fun broadcastMessage(sender: String, message: String, recipients: List<User>) {
    SESSIONS.filter { ctx: WsContext -> ctx.session.isOpen }
        .forEach { session: WsContext ->
            session.send(
                """
                    {
                      "userMessage": {"sender": "$sender", "message": "$message"},
                      "userList": ${Json.encodeToString(ListSerializer(User.serializer()), recipients)}
                    }
                """.trimIndent()
            )
        }
}
