package dublduk.krautchat.model

import kotlinx.serialization.Serializable

@Serializable
data class InitialMessage(val title: String, val message: String, val tags: List<String> = emptyList())
