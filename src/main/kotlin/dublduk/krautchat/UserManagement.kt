package dublduk.krautchat

import dublduk.krautchat.model.User
import dublduk.krautchat.repos.UserRepository

class UserManagement(private val userRepository: UserRepository) {
    fun addUser(user: User) {
        userRepository.add(user)
    }

    fun removeUser(userId: String) {
        userRepository.deleteById(userId)
    }

    fun findUser(userId: String): User? {
        return userRepository.findById(userId)
    }

    fun all(): List<User> {
        return userRepository.listAll()
    }
}
