package dublduk.krautchat.repos

import dublduk.krautchat.model.InitialMessage

interface MessageRepository {
    fun addInitMessage(initialMessage: InitialMessage): String

    fun messageById(id: String): InitialMessage?

    fun messagesByTag(tag: String): List<InitialMessage>
}
