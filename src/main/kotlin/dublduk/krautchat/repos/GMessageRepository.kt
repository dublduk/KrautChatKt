package dublduk.krautchat.repos

import dublduk.krautchat.model.InitialMessage
import org.apache.tinkerpop.gremlin.process.traversal.TraversalSource
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource
import org.apache.tinkerpop.gremlin.structure.Vertex
import java.util.concurrent.atomic.AtomicLong

// todo: add indexes for better performance
class GMessageRepository(private val g: GraphTraversalSource) : MessageRepository {
    override fun addInitMessage(initialMessage: InitialMessage): String {
        val graph = g.graph
        val tx = graph.tx()
        tx.begin<TraversalSource>()
        val id = generateId()
        val messageV = g.addV("initial_message")
            .property("id", id)
            .property("title", initialMessage.title)
            .property("message", initialMessage.message)
            .next()
        for (tag in initialMessage.tags) {
            val tagV = g.addV("tag")
                .property("value", tag)
                .next()
            g.addE("tag_edge").from(messageV).to(tagV)
                .next() // note: 'next' or 'property' must be called at the end of the chain
        }
        tx.commit()
        return id
    }

    override fun messageById(id: String): InitialMessage {
        val v = g.V().has("id", id).next()
        return map(v)
    }

    override fun messagesByTag(tag: String): List<InitialMessage> {
        return g.V().hasLabel("tag").has("value", tag)
            .`in`()
            .map { v -> v.get()?.let { map(it) } }
            .toList()
            // or kinda `.promise(::toList)`
            // but from javaDocs, the graph must be created using `withRemote(Configuration)` to use `promise`.
    }

    companion object {
        private val COUNTER = AtomicLong(0L)

        private fun generateId(): String {
            return COUNTER.getAndIncrement().toString()
        }

        private fun map(vertex: Vertex): InitialMessage {
            val title = vertex.property<String>("title").value()
            val message = vertex.property<String>("message").value()
            return InitialMessage(title, message)
        }
    }
}
