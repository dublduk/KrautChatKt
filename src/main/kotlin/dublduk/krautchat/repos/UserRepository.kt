package dublduk.krautchat.repos

import dublduk.krautchat.model.User

interface UserRepository {
    fun add(user: User)

    fun findById(id: String): User?

    fun deleteById(id: String)

    fun listAll(): List<User>
}
