package dublduk.krautchat.repos

import dublduk.krautchat.model.User
import java.util.concurrent.ConcurrentHashMap

object InMemUserRepository : UserRepository {
    private val DATA_STORE = ConcurrentHashMap<String, User>()

    override fun add(user: User) {
        DATA_STORE[user.id] = user
    }

    override fun findById(id: String): User? {
        return DATA_STORE[id]
    }

    override fun deleteById(id: String) {
        DATA_STORE.remove(id)
    }

    override fun listAll(): List<User> {
        return DATA_STORE.values.toList()
    }
}
