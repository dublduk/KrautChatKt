package dublduk.krautchat.repos

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal
import org.apache.tinkerpop.gremlin.structure.Vertex
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONMapper
import org.apache.tinkerpop.gremlin.structure.io.graphson.GraphSONVersion

object JacksonGraphSONV1Mapper : GraphMapper<String> {
    override fun map(traversal: GraphTraversal<Vertex, Vertex>): String {
        val mapper = GraphSONMapper.build()
            .version(GraphSONVersion.V1_0)
            .create()
            .createMapper()
        return mapper.writeValueAsString(traversal)
    }
}
