package dublduk.krautchat.repos

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal
import org.apache.tinkerpop.gremlin.structure.Vertex

interface GraphMapper<T> {
    fun map(traversal: GraphTraversal<Vertex, Vertex>): T
}
