package dublduk.krautchat

import dublduk.krautchat.db.CassandraGraphDb
import dublduk.krautchat.model.InitialMessage
import dublduk.krautchat.model.User
import dublduk.krautchat.repos.GMessageRepository
import dublduk.krautchat.repos.InMemUserRepository
import dublduk.krautchat.util.GDbLoader
import io.javalin.Javalin
import io.javalin.http.staticfiles.Location
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import java.util.concurrent.atomic.AtomicInteger

fun main(args: Array<String>) {
    val parser = ArgParser("chatty")
    val predefDataFile by parser.option(ArgType.String, shortName = "p", description = "Path to file with predefined data (GraphML, GraphSON or CSV)")
    val staticContentPath by parser.option(ArgType.String, shortName = "s", description = "Path to static content (HTML, JS, etc.)")
    parser.parse(args)

    val g = CassandraGraphDb.g
    predefDataFile?.let {
        GDbLoader(g).loadData(it)
    }

    val chat = Chat(GMessageRepository(g))
    val userMgmt = UserManagement(InMemUserRepository)

    val app = Javalin.create { cfg ->
        cfg.showJavalinBanner = false
        staticContentPath?.let {
            cfg.addStaticFiles(it, Location.EXTERNAL)
        }
    }.start(7070)
    app.get("/chat/search") { ctx ->
        val reqBody = ctx.body()
        val messages = chat.searchByTag(reqBody)
        ctx.result(Json.encodeToString(ListSerializer(InitialMessage.serializer()), messages))
    }

    val counter = AtomicInteger(0) // Assign to username for next connecting user

    app.ws("/chat") { ws ->
        // Temporary solution to add/remove users on WS connect/disconnect
        ws.onConnect { ctx ->
            val user = User(ctx.sessionId, "Elvis${counter.getAndIncrement()}")
            userMgmt.addUser(user)
            ctx.addClient("${user.username} is here!", userMgmt.all())
        }
        ws.onClose { ctx ->
            val user = userMgmt.findUser(ctx.sessionId)
            userMgmt.removeUser(ctx.sessionId)
            ctx.removeClient("${user?.username ?: "unknown"} left the building...", userMgmt.all())
        }
        ws.onMessage { ctx ->
            chat.addInitMessage(ctx.sessionId, ctx.message())
            val user = userMgmt.findUser(ctx.sessionId)
            sendMessage(user?.username ?: "unknown", ctx.message(), userMgmt.all())
        }
    }
}
