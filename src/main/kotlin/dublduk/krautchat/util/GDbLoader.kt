package dublduk.krautchat.util

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource

class GDbLoader(private val g: GraphTraversalSource) {
    fun loadData(filepath: String) {
        g.io<Any>(filepath).read().iterate()
    }
}
