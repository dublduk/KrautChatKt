package dublduk.krautchat.db

import org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource

object InMemGraphDb {
    val g: GraphTraversalSource by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        traversal().withRemote("conf/remote-graph.properties")
    }
}
