package dublduk.krautchat.db

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource
import org.janusgraph.core.JanusGraphFactory

object HBaseGraphDb {
    val g: GraphTraversalSource by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val janusGraph = JanusGraphFactory.build()
            .set("storage.backend", "hbase")
            .open()
        janusGraph.traversal()
    }
}
