package dublduk.krautchat.db

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource
import org.janusgraph.core.JanusGraphFactory

object CassandraGraphDb {
    // The variable g, the TraversalSource, only needs to be instantiated once and should then be re-used.
    val g: GraphTraversalSource by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val janusGraph = JanusGraphFactory.build()
            .set("storage.backend", "cql")
            .open()
        janusGraph.traversal()
    }
}
