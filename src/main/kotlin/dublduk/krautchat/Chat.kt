package dublduk.krautchat

import dublduk.krautchat.model.InitialMessage
import dublduk.krautchat.repos.MessageRepository

class Chat(private val messageRepository: MessageRepository) {
    fun searchByTag(tag: String): List<InitialMessage> {
        return messageRepository.messagesByTag(tag)
    }

    fun addInitMessage(userId: String, message: String): String {
        return messageRepository.addInitMessage(InitialMessage("...", message, listOf("hello")))
    }
}
